# Back-End Developer Test

## Sending to the API
* Searching

There are mutliple ways to search the Movies database. If you want a list of movies you can just get request the default movies: 

(GET) */movies 

If you want to search a singular movie (and you know the title of the movie you can search).

(GET) */movies?title={title}

If you do not know the exact name of the movie you can search

(GET) */movies?search={partial title}

* Filtering

There are multiple ways to filter your query to the database

If you want to filter based off of ratings
(GET) */movies/filters?filter=ratings&rating={rating}

If you want to filter based off of category (Action, Animation, Children, Classics,Comedy, Documentary, Drama, Family, Foreign, Games, Horror, Music, New, Sci-Fi, Sports, Travel)
(GET) */movies/filters?filter=category&query={category}

If you want to filter based off of year 
(GET) */movies/filters?filter=release-year&year{year}

If you want to filter based off of actor 
(GET) */movies/filters?filter=actor&first-name={first-name}&second-name={second-name}

If you search by */movies/filters/ it will return all of the Movies

*Adding a movie

to add to a movie you will need to use (*required). This will search for the title first and if the title already exist it will update instead. 

(POST) */movies?title={*title}&description={description}&release-year={release-year}&language-id={*language-id}&original-language-id={original-language-id}&=rental-duration={*rental-duration}&rental-rate={*rental-rate}&length={length}&replacement={*replacement-cost}&rating={rating}&special-features={special-features:'Trailers','Commentaries','Deleted Scenes','Behind the Scenes'}


## Connecing to the DB

We switched up the way you store the password for connecting to the DB. I added in a key / hash combo in order to get in. What you will need to do, is put the password you want to use inside of /Auth/Auth.php -- Encrypt function. After that, you'll need to call the DB TWICE, so that it makes both the key (stored in /Auth/Key/key.txt) and the info-hash stored in /Auth/Info/info. Running the connection twice but storing the password anywhere inside of the root folder, is not ideal. 

### Database Connection Information

__host__: 127.0.0.1<br />
__username__: sakila<br />
__password__: sakila<br />
__database__: sakila<br />
__port__: 3306

