<?php
namespace DataLayer;

class MovieData
{
	private \PDO $db;

	public function __construct(\PDO $db)
	{
		$this->db = $db;
	}
	//this is an if/else statement to figure out what get/movies is responding with
	public function getMovies($args): array
	{
		//if it is ?title= it will display the title associated 
		if($args["title"])
		{
			$stmt = $this->db->prepare('SELECT * FROM film WHERE film.title = "'.$args["title"].'"');
			$results = $stmt->execute();
		}
		//if it is ?search= it will find titles similar to that
		else if($args["search"])
		{
			$stmt = $this->db->prepare('SELECT * FROM film WHERE film.title LIKE "%'.$args["search"].'%"');
			$results = $stmt->execute();
		}
		//if there is nothing, respond with all
		else
		{
			$stmt = $this->db->prepare('select * from film');
			$stmt->execute();
		}
		//respond with all the movies gathered
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
	public function getFilterByElements($args): array
	{
		//switch statement to figure out what we are filtering by
		switch($args["filter"])
		{
			//if filters?ratings= will show those who equal the query
			case "ratings":
				$stmt = $this->db->prepare('SELECT * FROM film WHERE film.rating = "'.$args["rating"].'"');
				var_dump($stmt);
				break;
			//if filters?category= will show those who equal the query
			case "category":
				$stmt = $this->db->prepare('SELECT * FROM film WHERE film.film_id in (SELECT film_id from film_category WHERE film_category.category_id = (SELECT category_id FROM category WHERE category.name="'.$args["query"].'"))' );
				break;
			//if filters?release-years= will show those who equal the query
			case "release-year":
				$stmt = $this->db->prepare('SELECT * FROM film WHERE film.release_year = '.$args["year"].'');
				break;
			//if filters?actor= will show those who equal the query
			case "actor":
				$stmt = $this->db->prepare('SELECT * FROM film WHERE film.film_id in (SELECT film_id from film_actor WHERE film_actor.actor_id = (SELECT actor_id FROM actor WHERE actor.first_name="'.$args["first-name"].'" && actor.last_name="'.$args["last-name"].'"))' );
				break;
			default:
				$stmt = $this->db->prepare('select * from film');

		}
		//execute the statement
		$results = $stmt->execute();
		//fetches the movies that meet the response. 
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}

	//this is the post/movies
	public function postAMovie($args): array
	{
		//we are pulling in the variables from the URI
		$title = $args['title'];
		$description = $args['description'];
		$release_year = $args['release-year'];
		$language_id = $args['language-id'];
		$original_language_id = $args['original-language-id'];
		$rental_duration = $args['rental-duration'];
		$rental_rate = $args['rental-rate'];
		$length = $args['length'];
		$replacement_cost = $args['replacement'];
		$rating = $args['rating'];
		$special_features=$args['special-features'];

		//we are checking to see if the title already exists 
		$chkstmt = $this->db->prepare('SELECT title FROM film where film.title ="'.$arg['title'].'"');

		//if it does exist update instead of insert/create
		if($chkstmt->execute())
		{
			$stmt = $this->db->prepare('UPDATE film SET film.description="'.$description.'",release_year='.$release_year.',language_id='.$language_id.',original_language_id='.$original_language_id.',rental_duration='.$rental_duration.', film.length='.$length.',replacement_cost='.$replacement_cost.', rating="'.$rating.'",special_features="'.$special_features.'" 
			WHERE film.title ="'.$title.'"');

			echo "Film already existed; it has been updated";
		}
		//else create a new entry on film
		else{
			$stmt = $this->db->prepare('INSERT INTO film (title, film.description, release_year, language_id, original_language_id,rental_duration,rental_rate,film.length, replacement_cost, rating, special_features)
			VALUES ("'.$title.'","'.$description.'",'.$release_year.','.$language_id.','.$original_language_id.','.$rental_duration.','.$rental_rate.','.$length.','.$replacement_cost.',"'.$rating.'","'.$special_features.'")');

			echo "Film added";
		}
		$results = $stmt->execute();
		return $stmt->fetchAll(\PDO::FETCH_ASSOC);
	}
}
