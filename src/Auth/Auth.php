<?php
function create_key(){

    // Create The First Key
    $key1 = base64_encode(openssl_random_pseudo_bytes(32));
    $file = "./Auth/Key/key";
    $myfile = fopen($file, 'x+') or die("Unable to open file!");

    fwrite($myfile, $key1);
    fclose($myfile);
}

function get_key(){
  return  file_get_contents('./Auth/Key/key');
}

function encrypt($statement){

    file_exists('./Auth/Key/key') == false ? create_key() : $key = get_key();
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = openssl_random_pseudo_bytes($ivlen);
    $ciphertext_raw = openssl_encrypt($statement, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $hmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    $ciphertext = base64_encode( $iv.$hmac.$ciphertext_raw );

    $file = "./Auth/Info/info";
    $myfile = fopen($file, 'w+') or die("Unable to open file!");
    fwrite($myfile, $ciphertext);
    fclose($myfile);
}

function decrypt(){
    
    $key = get_key();

    $ciphertext = get_info();
    $c = base64_decode($ciphertext);
    $ivlen = openssl_cipher_iv_length($cipher="AES-128-CBC");
    $iv = substr($c, 0, $ivlen);
    $hmac = substr($c, $ivlen, $sha2len=32);
    $ciphertext_raw = substr($c, $ivlen+$sha2len);
    $original_plaintext = openssl_decrypt($ciphertext_raw, $cipher, $key, $options=OPENSSL_RAW_DATA, $iv);
    $calcmac = hash_hmac('sha256', $ciphertext_raw, $key, $as_binary=true);
    if (hash_equals($hmac, $calcmac))//PHP 5.6+ timing attack safe comparison
    {
        return $original_plaintext;
    }
    return $original_plaintext;
}
function get_info()
{
    $file = "./Auth/Info/info";
    return file_get_contents($file);
}
function store_info()
{
    encrypt("sakila");
}
?>