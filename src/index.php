<?php
require './vendor/autoload.php';
require './Auth/Auth.php';
$app = new \Slim\App;
$container = $app->getContainer();

/**
 * Container Definitions
 */

$container['db'] = function($c) {
	//We have a security check here to move the password out of index.php
	!file_exists('./Auth/Info/info.txt')? store_info() : null;
	$database = $user =  "sakila";
	//this calls a function to decrypt the password.
	$password = decrypt();
	$host = "mysql";
	//PDO call
	$db = new PDO("mysql:host={$host};dbname={$database};charset=utf8", $user, $password);
	$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	//remove emulated prepared statements to remove an SQL injection surface
	$db->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
	return $db;
};

$container['movieData'] = function($c) {
	return new \DataLayer\MovieData($c['db']);
};

/**
 * Routes
 */
//Our two get calls; we do a lot more on the Datalayer to determine what to do
$app->get('/movies', \Controllers\MovieController::class . ':listMovies');
//filter argument
$app->get('/movies/filters', \Controllers\MovieController::class . ':filterByElements', $args);

//how we create the movies
$app->post('/movies', \Controllers\MovieController::class . ':addMovies');

$app->run();
