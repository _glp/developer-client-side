<?php
namespace Controllers;

use Psr\Container\ContainerInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;

use \DataLayer\MovieData;

class MovieController
{
	private MovieData $movieData;

	public function __construct(ContainerInterface $container)
	{
		$this->movieData = $container->get('movieData');
	}
	//this is connected to the get/movies
	public function listMovies(Request $request, Response $response, array $args)
	{
		$args = $request->getQueryParams();
		$movies = $this->movieData->getMovies($args);
		return $response->withJson($movies);
	}
	//this is connected to the get/filter
	public function filterByElements(Request $request, Response $response, array $args)
	{
		$args = $request->getQueryParams();
		$movies = $this->movieData->getFilterByElements($args);
		return $response->withJson($movies);
	}
	//this is connected to the put/movie
	public function addMovies(Request $request, Response $response, array $args)
	{
		$args=$request->getQueryParams();
		$movies = $this->movieData->postAMovie($args);
		return $response->withJson($movies);
	}
	
}
